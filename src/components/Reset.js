import React from "react";
import img0 from '../Images/car.png';
import img2 from '../Images/user12.png';
import './Reset.css';
import { Link } from "react-router-dom";


function Reset() {
    return (
        <>
            <h1 className='heading2'><img src={img0} alt="carimg" />Free Rides</h1>

            <div className="outer2">

                <form >
                    <h1 className="admin2">Reset Password</h1>

                    <div className="password">
                        <img src={img2} alt="" />
                        <label>
                            <input
                                type="text"
                                name="password"
                                placeholder="Enter Your Password "
                                required="required"
                            ></input>
                        </label>
                    </div>
                    <button type="submit" className="btno">Save Password</button>
                </form>

                <div >
                    <Link className="back" to={`/`}> Back to login</Link>
                </div>

            </div>
        </>
    )
}

export default Reset;