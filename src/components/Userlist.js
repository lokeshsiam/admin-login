import React from "react";
import Sidebar from "./Sidebar";
import List from "./List";
import "./Userlist.css";
import Navbar from "./Navbar";

function Userlist() {
    return (
        <>
        <Navbar />
            <div className="dashboard">

                <div className="flex1">
                    <Sidebar />
                </div>

                <div className="flex2">
                    <h2>User</h2>
                    <List />
                </div>
            </div>
        </>
    )
}

export default Userlist;