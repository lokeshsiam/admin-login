import React from "react";
import img0 from '../Images/car.png';
import img1 from '../Images/user11.png';
import img2 from '../Images/user12.png';
import { Link } from "react-router-dom";
import './Login.css';


function Login() {

    return (
        <>
            <h1 className='heading'>
                <img src={img0} alt="carimg" />
                Free Rides</h1>

            <div className="outer">
                <form >
                    <h1 className="admin">Admin login</h1>
                    <div className="username">
                        <img src={img1} className="userimg" alt="" />
                        <label className="un">
                            <input
                                type="text"
                                name="username"
                                placeholder="Username"
                                required="required"
                            ></input>
                        </label>
                    </div>

                    <br />

                    <div className="password">
                        <img src={img2} alt="" />
                        <label>
                            <input
                                type="text"
                                name="password"
                                placeholder="Password"
                                required="required"
                            ></input>
                        </label>
                    </div>

                    <div ><Link className="forget" to="/reset">Forget Password ?</Link></div>

                    <Link className="Userlist" to="/Userlist"><button type="submit" className="Login">Log in</button></Link>
                </form>
            </div>
        </>
    )
}

export default Login;