import React from "react";
import Login from "./Login";
import Reset from "./Reset";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import Userlist from "./Userlist";

function Main() {
  return (

    <>
      <Router>
        <Switch>
          <Route exact path="/" component={Login}></Route>
          <Route exact path="/reset" component={Reset}></Route>
          <Route exact path="/Userlist" component={Userlist}></Route>
        </Switch>
      </Router>

     
    </>
  )
}

export default Main;